var gulp        = require('gulp')
var browserify  = require('browserify')
var babelify    = require('babelify')
var source      = require('vinyl-source-stream')
var uglifyify   = require('uglifyify')

gulp.task('build', function () {
  return browserify({entries: './app.jsx', debug: true})
    .transform('babelify', {presets: ['es2015', 'stage-0', 'react']})
    .transform('uglifyify')
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(gulp.dest('dist'))
})

gulp.task('watch', ['build'], function () {
  gulp.watch('*.jsx', ['build'])
  gulp.watch('./components/*.jsx', ['build'])
  gulp.watch('./stores/*.jsx', ['build'])
  gulp.watch('./actions/*.jsx', ['build'])
})

gulp.task('default', ['watch'])
