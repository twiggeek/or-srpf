import AppDispatcher from '../dispatcher/AppDispatcher'
import {TextConstants} from '../constants/constants.jsx'
import {EventEmitter} from 'events'
import assign from 'object-assign'

var CHANGE_EVENT = 'change'

var _lang = 'fr'
var _datatext = {
  fr : require('./datatext_fr.json'),
  en : require('./datatext_en.json')
}

function select (lang, done) {
  TextStore.select(lang)
  done()
}

var TextStore = assign({}, EventEmitter.prototype, {
  select: function (lang) {
    _lang = lang
  },
  getLanguage: function () {
    return _lang
  },
  getDataText: function () {
    return _datatext[_lang]
  },
  emitChange: function() {
    this.emit(CHANGE_EVENT)
  },
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback)
  },
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback)
  }
})

AppDispatcher.register(function(action) {
  switch(action.actionType) {

    case TextConstants.TEXT_SELECT:
      select(action.data.lang, function () {
        TextStore.emitChange()
      })
      break
  }
})

module.exports = TextStore
