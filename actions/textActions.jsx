import AppDispatcher from '../dispatcher/AppDispatcher'
import {TextConstants} from '../constants/constants.jsx'

export default class TextActions {
  select(lang) {
    AppDispatcher.dispatch({
      actionType: TextConstants.TEXT_SELECT,
      data: {lang : lang}
    })
  }
}
