import React from 'react'

import Content from './content.jsx'
import TextStore from '../stores/text.jsx'

function getTextState () {
  return {
    datatext: TextStore.getDataText()
  }
}

export default class index extends React.Component {
  static propTypes = {
    name: React.PropTypes.string,
  }

  state = getTextState()

  constructor(props) {
    super(props)
    TextStore.addChangeListener(this._onChange)
  }

  componentWillUnmount() {
    TextStore.removeChangeListener(this._onChange)
  }

  render() {
    return (
      <div id="wrapper">
        <Content datatext={this.state.datatext} />
      </div>
    )
  }

  _onChange() {
    this.setState(getTextState())
  }
}

