import React from 'react'
import {InputForm} from './formElements.jsx'

class FleetForm extends React.Component {
  static propTypes = {
    name: React.PropTypes.string,
  }

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className="panel">
        <div className="panel-heading">
          {this.props.title}
        </div>
        <div className="panel-body pan">
          <form>
            <div className="form-body pal">
              <div className="row">
                Hello
              </div>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

class InfoForm extends React.Component {
  static propTypes = {
    name: React.PropTypes.string,
  }

  constructor(props) {
    super(props)
  }

  _onChange() {

  }

  render() {
    return (
      <div className="panel">
        <div className="panel-heading">
          {this.props.title}
        </div>
        <div className="panel-body pan">
          <form className="form-horizontal">
            <div className="form-body pal">
              <InputForm  label={this.props.datatext.login}
                            placeholder={this.props.datatext.login}
                            value={''}
                            attrKey="login"
                            icon="user"
                            onChange={this._onChange} />
              </div>
          </form>
        </div>
      </div>
    )
  }
}

export default class SimulatorForm extends React.Component {
  static propTypes = {
    name: React.PropTypes.string,
  }

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className="row">
        <div className="col-lg-6">
          <InfoForm
            datatext={this.props.datatext}
            title={this.props.datatext.infoAttackerTitle} />
          <FleetForm
            datatext={this.props.datatext}
            title={this.props.datatext.fleetAttackerTitle} />
        </div>
        <div className="col-lg-6">
          <InfoForm
            datatext={this.props.datatext}
            title={this.props.datatext.infoDefenderTitle} />
          <FleetForm
            datatext={this.props.datatext}
            title={this.props.datatext.fleetDefenderTitle} />
        </div>
      </div>
    )
  }
}
