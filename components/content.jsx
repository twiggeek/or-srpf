import React from 'react'
import SimulatorForm from './simulatorForm.jsx'

export default class Content extends React.Component {
  static propTypes = {
    name: React.PropTypes.string,
  }

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div id="page-wrapper">
        <div className="page-title-breadcrumb">
          <div className="page-header pull-left">
            <div className="page-title">{this.props.datatext.title}</div>
          </div>
            <div className="clearfix"></div>
        </div>
        <div className="page-content">
          <div id="tab-general">
            <div className="row mbl">
              <div className="col-lg-6">
                <SimulatorForm datatext={this.props.datatext} />
              </div>
              <div className="col-lg-6">
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
