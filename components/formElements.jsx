var React = require('react')
var ReactPropTypes = React.PropTypes

var InputForm = React.createClass({
  propTypes: {
    label: ReactPropTypes.string.isRequired,
    attrKey: ReactPropTypes.string.isRequired,
    onChange: ReactPropTypes.func.isRequired,
    placeholder: ReactPropTypes.string,
    type: ReactPropTypes.string
  },
  getInitialState: function () {
    return {
      value: this.props.value || '',
      type: this.props.type || 'text'
    }
  },
  componentWillReceiveProps: function(nextProps) {
    this.setState(nextProps)
  },
  render: function () {
    return (
      <div className="form-group">
        <label className="col-md-3 control-label">{this.props.label}</label>
        <div className="col-md-9">
          <div className="input-icon right">
            <i className={"fa fa-" + this.props.icon}></i>
            <input type={this.state.type}
              placeholder={this.props.placeholder}
              value={this.state.value}
              onChange={this._onChange}
              className="form-control" />
          </div>
        </div>
      </div>
    )
  },
  reset: function () {
    this.setState({ value : '' });
  },
  _onChange: function (event) {
    this.props.onChange(this.props.attrKey, event.target.value)
    this.setState({ value: event.target.value })
  }
})

var TextareaForm = React.createClass({
  propTypes: {
    label: ReactPropTypes.string.isRequired,
    attrKey: ReactPropTypes.string.isRequired,
    onChange: ReactPropTypes.func.isRequired,
    placeholder: ReactPropTypes.string,
    value: ReactPropTypes.string,
  },
  getInitialState: function () {
    return {
      value: this.props.value || '',
    }
  },
  componentWillReceiveProps: function(nextProps) {
    this.setState(nextProps)
  },
  render: function () {
    return (
      <div className="form-group input-group">
        <span className="input-group-addon">{this.props.label}</span>
        <textarea className="form-control"
          placeholder={this.props.placeholder}
          value={this.state.value}
          onChange={this._onChange} />
      </div>
    )
  },
  reset: function () {
    this.setState({ value : '' });
  },
  _onChange: function (event) {
    this.props.onChange(this.props.attrKey, event.target.value)
    this.setState({ value: event.target.value })
  }
})

var RadioForm = React.createClass({
  propTypes: {
    label: ReactPropTypes.string.isRequired,
    attrKey: ReactPropTypes.string.isRequired,
    onChange: ReactPropTypes.func.isRequired,
    values: ReactPropTypes.array
  },
  getInitialState: function () {
    return {
      values : this.props.values || [{label : 'Yes', value : true}, {label : 'No', value : false}],
      selected : true
    }
  },
  componentWillReceiveProps: function(nextProps) {
    this.setState(nextProps)
  },
  render: function () {
    var radios = [];
    for (var key in this.state.values) {
      var checked = this.state.values[key].value == this.state.selected ? 'checked' : ''
      radios.push(<label key={key} className="radio-inline">
          <input type="radio" name={this.props.attrKey} value={this.state.values[key].value} onChange={this._onChange} checked={checked}/>
          {this.state.values[key].label}
        </label>)
    }
    return (
      <div className="form-group input-group">
        <label className="input-group-addon">{this.props.label}</label>
        {radios}
      </div>
    )
  },
  reset: function () {
    this.setState({ values : [{label : 'Yes', value : true}, {label : 'No', value : false}] });
  },
  _onChange: function (event) {
    this.props.onChange(this.props.attrKey, event.target.value)
    this.setState({ selected: event.target.value })
  }
})

module.exports = {
  InputForm : InputForm,
  TextareaForm : TextareaForm,
  RadioForm : RadioForm
}
